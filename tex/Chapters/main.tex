\chapter{Das Stochastic-Average-Gradient Verfahren}

Das von \cite{Roux2012ASG} eingeführte Stochastic-Average-Gradient Verfahren (SAG-Verfahren) ist eine besonders interessante Erweiterung des stochastischen Gradientenverfahrens, weil sie die Konvergenzgeschwindigkeit des Verfahrens unter der Zusatzannahme, dass die Funktionen von einem endlichen Datensatz stammen, erhöht. Im Gegensatz zum stochastischem Gradientenverfahren, welches sublinear konvergiert, konvergiert das SAG-Verfahren linear. 

Das SAG Verfahren ist durch die Rekursion
\begin{equation}\label{eq:sag}
\begin{aligned}
x^0 &\in \mathbb{R}^p\\
x^{k+1} &= x^k - \frac{\alpha_k}{n} \sum_{i=1}^n y_i^{k+1}
\end{aligned}
\end{equation}
gegeben, wobei 
\begin{equation}\label{eq:y_sag}
y_i^{k+1} := 
\begin{cases}
\nabla f_i(x^{k}) & \mbox{wenn } i = i_k, \\
y_i^{k} & \mbox{sonst},
\end{cases}
\end{equation}
und $i_k$ gleichverteilt aus $\{1, \dots, n\}$ gewählt wird. 
Anstatt wie im traditionellen stochastischen Gradientenverfahren die in den vorherigen Schritten berechneten Teilgradienten zu verwerfen, werden deren aktuellsten Versionen in $y^k$ mitgeführt. Die Verbesserung der Konvergenzgeschwindigkeit erfordert also zusätzlichen Speicherplatz. Im einfachsten Fall wird $y_i^0 = 0$ für $i$ in $\{1, \dots, n\}$ gewählt, aber es sind auch andere Initialisierungen für den Speicher der Teilgradienten möglich.  

\section{Schrittkosten}
%Wie beim stochastischem Gradientenverfahren, hängen die Berechnungskosten für einen einzelnen Schritt nicht von $n$ ab, da pro Iteration nur der Gradient einer Teilfunktion berechnet werden muss.
Wie beim stochastischen Gradientenverfahren hängen die Schrittkosten hauptsächlich von der Berechnung des Gradienten der Teilfunktion $\nabla f_i(x^k)$ ab, zusätzlich muss in jedem Schritt die Summe $\sum_{i=1}^n y_i^{k+1}$ gebildet werden. 

Beim Beispiel des Lösens von überbestimmten Gleichungssystemen, zur Erinnerung:
\begin{equation*}
\begin{aligned}
g(x) &= \frac{1}{2n}\|A x - b\|^2 \\
&= \frac{1}{2n} \sum_{i=1}^n |A_{i-}^T x - b_i|^2, A \in \mathbb{R}^{n\times p}
\end{aligned}
\end{equation*}
und
\begin{equation}
\nabla f_{i}(x) = A_{i-}^T(A_{i-} x - b_i),
\end{equation}
ergibt das einen Rechenaufwand in $\mathcal{O}(n + p)$, $n$ Operationen für die Summe $\sum_{i=1}^n y_i^k$ und $p$ für die Berechnung des Gradienten der Teilfunktion. Im Vergleich dazu liegt der Rechenaufwand beim traditionelle Gradientenverfahren $\mathcal{O}(n p)$ und beim stochastische Gradientenverfahren $\mathcal{O}(p)$. Der Aufwand der Iterationen des SAG-Verfahren ist zwischen dem der beiden anderen Verfahren. Allerdings liegt der Aufwand des SAG-Verfahrens für praxisrelevante Dimensionen von $n$ und $p$ deutlich näher bei dem des stochastischen Gradientenverfahren.  

%Zusatz https://de.wikipedia.org/wiki/Kaczmarz-Methode
\section{Theoretische Konvergenzanalyse}

Folgender Satz zeigt die bereits erwähnte Q-lineare Konvergenz des SAG-Verfahrens.
\begin{proposition}\emph{(Konvergenzrate SAG-Verfahren)}\label{prop:sag}
Mit der konstanten Schrittweite $\alpha_k = \frac{1}{2 n L}$ konvergiert das SAG-Verfahren exponentiell und die durch Gleichung \eqref{eq:sag} und Gleichung \eqref{eq:y_sag} gegebenen Iterierten erfüllen für $k > 1$ die Eigenschaft:
\begin{equation}
\mathbb{E}\left[\|x^k - x^*\|^2\right] \leq \left(1- \frac{\mu}{8 L n}\right)^k \left[3\|x_0 - x^*\|^2 + \frac{9\sigma^2}{4L^2}\right],
\end{equation}
wobei $n$ die Anzahl der Teilfunktionen, $L$ eine Lipschitzkonstante der Teilgradienten, $\mu$ die zur stark konvexen Funktion $g$ gehörige Konstante und $\sigma^2 := \frac{1}{n} \sum_{i=1}^n \|f_i(x^*)\|^2$ ist.
\end{proposition}
\begin{proof}\renewcommand{\qedsymbol}{}
Im Folgenden werden wir den Beweis der Konvergenzrate von \cite{Roux2012ASG} nachvollziehen. 
\end{proof}


\subsection{Problemformulierung und Notation}

Wir betrachten die $\mu$-stark konvexe Funktion $g = \frac{1}{n}\sum_{i=1}^n f_i$, wobei die Funktionen $f_1, \dots, f_n$ konvexe Funktionen von $\mathbb{R}^p$ nach $\mathbb{R}$ mit $L$-Lipschitz stetigen Gradienten sind. Sei $x^*$ die eindeutige Minimalstelle von $g$.

Für $k\geq 1$ wird beim SAG-Verfahren die Rekursion
$$
x^k = x^{k-1} - \frac{\alpha}{n} \sum_{i=1}^n y_i^k,
$$
wobei $i_k$ gleichverteilt aus $\{1, \dots, n\}$ gewählt wird und wir setzen 
$$y_i^k = 
\begin{cases}
\nabla f_i(x^{k-1}) & \mbox{wenn } i = i_k, \\
y_i^{k-1} & \mbox{sonst}.
\end{cases}
$$
Bezeichne $z_i^k$ eine Zufallsvariable, die den Wert $1-\frac{1}{n}$ mit Wahrscheinlichkeit $\frac{1}{n}$ annimmt und den Wert $-\frac{1}{n}$ mit Wahrscheinlichkeit $\frac{n-1}{n}$, so kann $y_i^k$ auch als
$$
y_i^k = \left(1 - \frac{1}{n}\right) y_i^{k-1} + \frac{1}{n} \nabla f_i(x^{k-1}) + z_i^k \left[\nabla f_i(x^{k-1}) - y_i^{k-1}\right]
$$
ausgedrückt werden. Denn für den Fall $z_i^k = 1-\frac{1}{n}$ gilt die Gleichung
\begin{equation*}
\begin{aligned}
y_i^k &= \left(1 - \frac{1}{n}\right) y_i^{k-1} + \frac{1}{n} \nabla f_i(x^{k-1}) + \left(1 - \frac{1}{n}\right) \left[\nabla f_i(x^{k-1}) - y_i^{k-1}\right]\\
&= \nabla f_i(x^{k-1})
\end{aligned}
\end{equation*}
und für den Fall $z_i^k = -\frac{1}{n}$ gilt die Gleichung
\begin{equation*}
\begin{aligned}
y_i^k &= \left(1 - \frac{1}{n}\right) y_i^{k-1} + \frac{1}{n} \nabla f_i(x^{k-1}) -\frac{1}{n} \left[\nabla f_i(x^{k-1}) - y_i^{k-1}\right]\\
&= y_i^{k-1}.
\end{aligned}
\end{equation*}
Setzt man diese Darstellung in die Definition von $x^k$ ein, so erhält man
$$
x^k = x^{k-1} - \frac{\alpha}{n} \sum_{i=1}^n \left[\left(1 - \frac{1}{n}\right) y_i^{k-1} + \frac{1}{n} \nabla f_i(x^{k-1}) + z_i^k \left[\nabla f_i(x^{k-1}) - y_i^{k-1}\right]\right].
$$
Aufgrund der Zugehörigkeit von $y_i^k$ und $\nabla f_i(x^{k-1})$ zu $\mathbb{R}^p$ und der Definition von $\nabla g$ kann $x^k$ als
$$
x^k = x^{k-1} - \frac{\alpha}{n} \sum_{i=1}^n \left[\left(1 - \frac{1}{n}\right)I y_i^{k-1} + z_i^k I \left[\nabla f_i(x^{k-1}) - y_i^{k-1}\right]\right] - \frac{\alpha}{n} \nabla g(x^{k-1})
$$
geschrieben werden, wobei $I$ die Einheitsmatrix in $\mathbb{R}^{p \times p}$ bezeichnet.  
Führt man zusätzlich die Matrizen 
$$
\begin{aligned} 
e = \begin{pmatrix}
I \\ \vdots \\ I 
\end{pmatrix}
\in \mathbb{R}^{np \times p},\ 
&
\nabla f(x) = \begin{pmatrix}
\nabla f_1(x) \\ \vdots \\ \nabla f_n(x)
\end{pmatrix}
\in \mathbb{R}^{np},\ 
&&
z^k = \begin{pmatrix}
z_1^k I \\ \vdots \\ z_n^k I 
\end{pmatrix}
\in \mathbb{R}^{np \times p}
\end{aligned}
$$
ein, so erhält man eine Matrixschreibweise für $x^k$, nämlich,
$$
x^k = x^{k-1} - \frac{\alpha}{n} \left[\left(1-\frac{1}{n}\right)e^T y^{k-1} + \nabla g(x^{k-1}) + (z^k)^T \left[\nabla f(x^{k-1})-y^{k-1}\right]\right].
$$
Mit obiger Definition von $z^k$ gilt $\mathbb{E}[z^k (z^k)^T] = \frac{1}{n}I - \frac{1}{n^2} e e^T$. Außerdem sind die Variablen $z_1^k, \dots, z_n^k$ für gegebenes $k$ nicht unabhängig. Die von den Zufallsvariablen $z^1, \dots, z^k$ induzierte $\sigma$-Algebra wird als $\mathcal{F}_k$ bezeichnet.

Des Weiteren nutzen wir die Notation
$$
\begin{aligned} 
\theta^k = \begin{pmatrix}
y_1^k \\ \vdots \\ y_n^k \\ x^k 
\end{pmatrix}
\in \mathbb{R}^{(n+1)p},\ 
&
\theta^* = \begin{pmatrix}
\nabla f_1(x^*) \\ \vdots \\ \nabla f_n(x^*) \\ x^*
\end{pmatrix}
\in \mathbb{R}^{(n+1)p}
\end{aligned}
$$
und
$$
\sigma^2 = \frac{1}{n} \sum_{i=1}^n \|\nabla f_i(x^*)\|^2.
$$

Ist $M$ eine $tp \times tp$ Matrix und $m$ eine $tp \times p$ Matrix, dann bezeichne
\begin{itemize}
\item $\operatorname{diag}(M)$ die $tp \times p$ Matrix, die die Konkatenierung der $t (p \times p)$-Blöcke auf der Diagonalen von $M$ enthält und
\item $\operatorname{Diag}(m)$ die $tp \times tp$ Matrix, deren $(p\times p)$-Blöcke auf der Diagonale den $(p\times p)$-Blöcken aus $m$ entsprechen.
\end{itemize}

\subsection{Beweis nach \cite{Roux2012ASG}}

Beim Beweis wird wie folgt vorgegangen:

\begin{enumerate}
\item Zunächst wird mithilfe einer sogenannten Lyapunov Funktion $Q$ von $\mathbb{R}^{(n+1)p}$ nach $\mathbb{R}$ eine lineare konvergente Folge $\left(\mathbb{E}Q(\theta^k)\right)_{k\in \mathbb{N}}$ konstruiert.
\item Anschließend wird gezeigt, dass die $k$-te Differenz $\|x^k - x^*\|^2$ vom $k$-ten Folgenglied $Q(\theta^k)$ um eine Konstante dominiert wird.
\end{enumerate}

\begin{definition}\emph{(Lyapunov Funktion nach \cite{lyapunov})}
Sei $q \in \mathbb{N}$ und $\mathcal{D} \subseteq \mathbb{R}^q$ offen. Eine Funktion $V: \mathcal{D} \to \mathbb{R}$ heißt \emph{Lyapunov Funktion}, wenn sie stetig differenzierbar und positiv definit, d.h. $V(y) > 0$ für $y \neq 0$, ist.
\end{definition}
$\hfill$
\newline
\textbf{1. Lineare Konvergenz der Lyapunov Funktion}
\newline
Für die konstante Schrittweite $\alpha = \frac{1}{2nL}$ betrachten wir die quadratische Lyapunov Funktion 
\begin{equation}
Q(\theta^k) = (\theta^k - \theta^*)^T \begin{pmatrix}A & b \\ b^T & c \end{pmatrix}(\theta^k - \theta^*),
\end{equation}
mit
\begin{equation}
\begin{aligned}
A &= 3n\alpha^2 I + \frac{\alpha^2}{n}\left(\frac{1}{n} - 2\right)ee^T\\
b &= -\alpha\left(1 - \frac{1}{n}\right)e\\
c &= I\\
S &= 3n\alpha^2I\\
b - \frac{\alpha}{n}ec &= -\alpha e.
\end{aligned}
\end{equation}
Zur Erinnerung $\theta^k$ und $\theta^*$ sind durch 
$$
\begin{aligned} 
\theta^k = \begin{pmatrix}
y_1^k \\ \vdots \\ y_n^k \\ x^k 
\end{pmatrix}
\in \mathbb{R}^{(n+1)p}\ 
&\text{ und } &&
\theta^* = \begin{pmatrix}
\nabla f_1(x^*) \\ \vdots \\ \nabla f_n(x^*) \\ x^*
\end{pmatrix}
\in \mathbb{R}^{(n+1)p}
\end{aligned}
$$
gegeben

Um die lineare Konvergenz der resultierenden Folge $\left(\mathbb{E}Q(\theta^k)\right)_{k\in\mathbb{N}}$ zu zeigen, muss ein $\delta > 0$ gefunden werden für welches die Ungleichung $\mathbb{E}Q(\theta^k) \leq (1-\delta)\mathbb{E}Q(\theta^{k-1})$ gilt. Dazu wird das nachfolgende Lemma, welches eine alternative Darstellung für den Ausdruck $$\mathbb{E}\left[(\theta^k - \theta^*)^T \begin{pmatrix}
A & b \\ b^T & c \end{pmatrix} (\theta^k - \theta^*)\vert \mathcal{F}_{k-1}\right]$$ liefert verwendet. Dabei bezeichnet $\mathbb{E}\left[X \vert \mathcal{F}_{k-1}\right]$ den bedingten Erwartungswert der Zufallsvariable $X$ gegeben der Unter-$\sigma$-Algebra $\mathcal{F}_{k-1}$. Die $\sigma$-Algebra $\mathcal{F}_{k-1}$ ist eine Unter-$\sigma$-Algebra von $\mathcal{F}_{k}$.

\begin{lemma} Für eine Matrix $P = \begin{pmatrix}
A & b \\ b^T & c \end{pmatrix}$, mit $A \in \mathbb{R}^{np \times np}$, $B \in \mathbb{R}^{np \times p}$ und $c \in \mathbb{R}^{p \times p}$, gilt
\begin{equation}
\begin{aligned}	
&\mathbb{E}\left[(\theta^k - \theta^*)^T \begin{pmatrix}
A & b \\ b^T & c \end{pmatrix} (\theta^k - \theta^*)\vert \mathcal{F}_{k-1}\right] \\&\ \ \ 
= (y^{k-1} - \nabla f(x^*))^T \left[\left(1 - \frac{2}{n}\right)S + \frac{1}{n}\operatorname{Diag}(\operatorname{diag}(S))\right](y^{k-1}-\nabla f(x^*)) \\&\ \ \ 
+ \frac{1}{n}(\nabla f(x^{k-1}) - \nabla f(x^*))^T \operatorname{Diag}(\operatorname{diag}(S))(\nabla f(x^{k-1}) - \nabla f(x^*))\\&\ \ \ 
+ \frac{2}{n}(y^{k-1} - \nabla f(x^*))^T[S - \operatorname{Diag}(\operatorname{diag}(S))](\nabla f(x^{k-1}) - \nabla f(x^*))\\&\ \ \ 
+ 2\left(1 - \frac{1}{n}\right)(y^{k-1} - \nabla f(x^*))^T\left[b - \frac{\alpha}{n}ec\right](x^{k-1} - x^*)\\&\ \ \ 
+ \frac{2}{n}(\nabla f(x^{k-1}) - \nabla f(x^*))^T\left[b - \frac{\alpha}{n}ec\right](x^{k-1} - x^*)\\&\ \ \ 
+ (x^{k-1} - x^*)^Tc(x^{k-1} - x^*),
\end{aligned}	
\end{equation}
mit 
\begin{equation}
S = A - \frac{\alpha}{n} b e^T - \frac{\alpha}{n} e b^T + \frac{\alpha^2}{n^2} e c e^T.
\end{equation}
\end{lemma}
\begin{proof}
Der interessierte Leser findet den Beweis für dieses Lemma im Anhang der Publikation von \cite{Roux2012ASG}.
\end{proof}

Mithilfe des obigen Lemmas erhalten wir
\begin{equation}\label{eq:lyapunov_expanded}
\begin{aligned}
\mathbb{E}\left[Q(\theta^k)\vert\mathcal{F}_{k-1}\right] &= \mathbb{E}\left[(\theta^k - \theta^*)^T \begin{pmatrix}
A & b \\ b^T & c \end{pmatrix} (\theta^k - \theta^*)\vert \mathcal{F}_{k-1}\right] \\
&= (y^{k-1} - \nabla f(x^*))^T \left[\left(1 - \frac{2}{n}\right)S + \frac{1}{n}\operatorname{Diag}(\operatorname{diag}(S))\right](y^{k-1}-\nabla f(x^*)) \\
&+ \frac{1}{n}(\nabla f(x^{k-1}) - \nabla f(x^*))^T \operatorname{Diag}(\operatorname{diag}(S))(\nabla f(x^{k-1}) - \nabla f(x^*))\\
&+ \frac{2}{n}(y^{k-1} - \nabla f(x^*))^T[S - \operatorname{Diag}(\operatorname{diag}(S))](\nabla f(x^{k-1}) - \nabla f(x^*))\\
&+ 2\left(1 - \frac{1}{n}\right)(y^{k-1} - \nabla f(x^*))^T\left[b - \frac{\alpha}{n}ec\right](x^{k-1} - x^*)\\
&+ \frac{2}{n}(\nabla f(x^{k-1}) - \nabla f(x^*))^T\left[b - \frac{\alpha}{n}ec\right](x^{k-1} - x^*)\\
&+ (x^{k-1} - x^*)^Tc(x^{k-1} - x^*).
\end{aligned}
\end{equation}
Für unsere Wahl von $A, b$ und $c$ gelten die Gleichungen
\begin{equation}\label{eq:lyapunov_sim1}
S - \operatorname{Diag}(\operatorname{diag}(S)) = 3n\alpha^2 I - 3n\alpha^2I = 0 
\end{equation}
und aufgrund der Konvexität von $g$
\begin{equation}\label{eq:lyapunov_sim2}
e^T(\nabla f(x^{k-1}) - \nabla f(x^*)) = n (\nabla g(x^{k-1}) - \underbrace{\nabla g(x^*)}_{=0}) = n\nabla g(x^{k-1}).
\end{equation}
Einsetzen von Gleichung \eqref{eq:lyapunov_sim1} in Gleichung \eqref{eq:lyapunov_expanded} liefert
\begin{equation}\label{eq:lyapunov_expanded_1}
\begin{aligned}
\mathbb{E}\left[Q(\theta^k)\vert\mathcal{F}_{k-1}\right] 
&=\left(1- \frac{1}{n}\right)3 n\alpha^2(y^{k-1} - \nabla f(x^*))^T(y^{k-1} - \nabla f(x^*))\\
&+(x^{k-1}-x^*)^T(x^{k-1}-x^*) - \frac{2\alpha}{n}(x^{k-1}-x^*)^Te^T(\nabla f(x^{k-1}) - \nabla f(x^*))\\
&+ 3\alpha^2(\nabla f(x^{k-1}) - \nabla f(x^*))^T(\nabla f(x^{k-1}) - \nabla f(x^*))\\
&-2\alpha\left(1 - \frac{1}{n}\right)(y^{k-1}-\nabla f(x^*))^T(x^{k-1}-x^*)
\end{aligned}
\end{equation}
und darauffolgendes einsetzen von Gleichung \eqref{eq:lyapunov_sim2} liefert
\begin{equation}\label{eq:lyapunov_expanded_2}
\begin{aligned}
\mathbb{E}\left[Q(\theta^k)\vert\mathcal{F}_{k-1}\right] 
&=\left(1- \frac{1}{n}\right)3 n\alpha^2(y^{k-1} - \nabla f(x^*))^T(y^{k-1} - \nabla f(x^*))\\
&+(x^{k-1}-x^*)^T(x^{k-1}-x^*) - \textcolor{red}{2\alpha}(x^{k-1}-x^*)^T \textcolor{red}{\nabla g(x^{k-1})}\\
&+ 3\alpha^2(\nabla f(x^{k-1}) - \nabla f(x^*))^T(\nabla f(x^{k-1}) - \nabla f(x^*))\\
&-2\alpha\left(1 - \frac{1}{n}\right)(y^{k-1}-\nabla f(x^*))^T(x^{k-1}-x^*).
\end{aligned}
\end{equation}
Nähere Betrachtung der dritten Zeile von Gleichung \eqref{eq:lyapunov_expanded_2} liefert
\begin{equation}\label{eq:lyapunov_sim3}
\begin{aligned}
(\nabla f(x^{k-1}) - \nabla f(x^*))^T(\nabla f(x^{k-1}) - \nabla f(x^*)) 
&= \sum_{i=1}^n \|\nabla f_i(x^{k-1}) - \nabla f_i(x^*)\|^2 \\
&\leq \sum_{i=1}^n L(\nabla f_i(x^{k-1}) - \nabla f_i(x^*))^T(x^{k-1} - x^*) \\
&= n L (\nabla g(x^{k-1}) - \nabla g(x^*))^T(x^{k-1} - x^*) \\
&= n L \nabla g(x^{k-1})^T(x^{k-1} - x^*),
\end{aligned}
\end{equation}
wobei die Ungleichung in der zweiten Zeile von \cite[Theorem 2.1.5]{nesterov} stammt. Einsetzen von Gleichung \eqref{eq:lyapunov_sim3} in Gleichung \eqref{eq:lyapunov_expanded_2} liefert
\begin{equation}\label{eq:lyapunov_expanded_3}
\begin{aligned}
\mathbb{E}\left[Q(\theta^k)\vert\mathcal{F}_{k-1}\right] 
&=\left(1- \frac{1}{n}\right)3 n\alpha^2(y^{k-1} - \nabla f(x^*))^T(y^{k-1} - \nabla f(x^*))\\
&+(x^{k-1}-x^*)^T(x^{k-1}-x^*) - 2\alpha(x^{k-1}-x^*)^T \nabla g(x^{k-1})\\
&+ 3\alpha^2\textcolor{red}{n L (x^{k-1} - x^*)^T \nabla g(x^{k-1})}\\
&-2\alpha\left(1 - \frac{1}{n}\right)(y^{k-1}-\nabla f(x^*))^T(x^{k-1}-x^*).
\end{aligned}
\end{equation}

Des Weiteren gilt per Definition
\begin{equation}
\begin{aligned}
(1-\delta)Q(\theta^{k-1}) 
&= (1-\delta) (\theta^{k-1} - \theta^*)^T \begin{pmatrix}
   A & b \\ b^T & c \end{pmatrix} (\theta^{k-1} - \theta^*)\\
&= (1-\delta) (y^{k-1} - \nabla f(x^*))^T\left[3n\alpha^2 I + \frac{\alpha^2}{n}
   \left(\frac{1}{n} - 2\right)ee^T\right](y^{k-1} - \nabla f(x^*))\\
&\ \ + (1-\delta)(x^{k-1}-x^*)^T(x^{k-1}-x^*)\\
&\ \ - 2\alpha (1-\delta)\left(1 - \frac{1}{n}\right)(y^{k-1} - \nabla f(x^*))^T e (x^{k-1}-x^*).
\end{aligned}
\end{equation}
Für die Differenz $\mathbb{E}[Q(\theta^k)\vert \mathcal{F}_{k-1}] - (1 - \delta)Q(\theta^{k-1})$ gilt die Abschätzung 
\begin{equation}\label{eq:lyapunov_absch_1}
\begin{aligned}
&\mathbb{E}[Q(\theta^k)\vert \mathcal{F}_{k-1}] - (1 - \delta)Q(\theta^{k-1}) \\
&\ \ \ \leq (y^{k-1} - \nabla f(x^*))^T\left[3n\alpha^2\left(\delta - \frac{1}{n}\right) I 
 + (1-\delta)\frac{\alpha^2}{n} \left(2 -\frac{1}{n}\right)ee^T\right](y^{k-1} - \nabla f(x^*))\\
&\ \ \ \ \ + \delta (x^{k-1}-x^*)^T(x^{k-1}-x^*) \\
&\ \ \ \ \ - (2\alpha - 3\alpha^2 n L)(x^{k-1} - x^*)^T\nabla g(x^{k-1}) \\
&\ \ \ \ \ - 2\alpha \delta \left(1 - \frac{1}{n}\right)(y^{k-1} - \nabla f(x^*))^T e (x^{k-1}-x^*).
\end{aligned}
\end{equation}

Diese Abschätzung kann mittels Eigenschaften negativ definiter Matrizen weiter verfeinert werden. Für eine negativ definite Matrix $M$ und zwei Vektoren $s$ und $t$ gilt nämlich
\begin{equation}
(s + \frac{1}{2} M^{-1} t)^T M (s + \frac{1}{2} M^{-1} t) \leq 0, 
\end{equation}
und deshalb insbesondere
\begin{equation}\label{eq:lyapunov_sim4}
s^T M s + s^Tt \leq -\frac{1}{4} t^T M^{-1} t.
\end{equation}
Die Verwendung von Ungleichung \eqref{eq:lyapunov_sim4} mit 
\begin{equation*}
\begin{aligned}
M &= \left[3n\alpha^2\left(\delta - \frac{1}{n}\right) I 
    + (1-\delta)\frac{\alpha^2}{n} \left(2 - \frac{1}{n}\right)ee^T\right] \\
  &= \left[3n\alpha^2\left(\delta - \frac{1}{n}\right) \left(I - \frac{ee^T}{n}\right) 
    + \alpha \left(3n\delta - 1 - 2\delta + \frac{\delta - 1}{n}\right)\frac{ee^T}{n}\right] \\
s &= y^{k-1} - \nabla f(x^*) \\
t &= -2\alpha\delta (1-\frac{1}{n})e(x^{k-1} - x^*) 
\end{aligned}
\end{equation*}
liefert 
\begin{equation}\label{eq:lyapunov_sim5}
\begin{aligned}
& (y^{k-1} - \nabla f(x^*))^T\left[3n\alpha^2\left(\delta - \frac{1}{n}\right) I 
 + (1-\delta)\frac{\alpha^2}{n} \left(2 - \frac{1}{n}\right)ee^T\right](y^{k-1} - \nabla f(x^*)) \\
&\ \ \  - 2\alpha \delta \left(1 - \frac{1}{n}\right)(y^{k-1} - \nabla f(x^*))^T e (x^{k-1}-x^*) \\
&\ \leq -\alpha^2 \delta^2 \left(1 - \frac{1}{n}\right)^2 (x^{k-1} - x^*)^T e^T 
   \Bigl[3n\alpha^2\left(\delta - \frac{1}{n}\right) \left(I - \frac{ee^T}{n}\right) 
    \\&\ + \alpha \left(3n\delta - 1 - 2\delta + \frac{\delta - 1}{n}\right)\frac{ee^T}{n}\Bigr]^{-1} e(x^{k-1} - x^*) \\
&\ = \frac{\alpha^2 \delta^2 \left(1 - \frac{1}{n}\right)^2 n}{\alpha^2 [3n\delta -1 -2\delta +\frac{\delta -1}{n}]} \|x^{k-1} - x^*\|^2 \\
&\ =  \frac{\delta^2 \left(1 - \frac{1}{n}\right)^2 n}{3n\delta -1 -2\delta +\frac{\delta -1}{n}} \|x^{k-1} - x^*\|^2,
\end{aligned}
\end{equation}
wobei anzumerken ist, dass $M$ für hinreichend kleine $\delta \leq \frac{1}{3n}$ negativ definit ist. Somit gilt Ungleichung \eqref{eq:lyapunov_sim5} für $\delta \leq \frac{1}{3n}$ und es folgt die neue Schranke
\begin{equation}
\begin{aligned}
\mathbb{E}[Q(\theta^k)\vert \mathcal{F}_{k-1}] - (1 - \delta)Q(\theta^{k-1}) 
&\leq -(2\alpha - 3\alpha^2 n L)(x^{k-1} - x^*)^T \nabla g(x^{k-1}) \\
&\ +\left(\delta - \frac{\delta^2 \left(1 - \frac{1}{n}\right)^2 n}{3n\delta -1 -2\delta +\frac{\delta -1}{n}}\right) \|x^{k-1} - x^*\|^2.
\end{aligned}
\end{equation}

An dieser Stelle wird die starke Konvexität von $g$ verwendet. Aus der starken Konvexität von $g$ folgt nämlich unmittelbar die Ungleichung
\begin{equation}\label{eq:lyapunov_sim6}
\|x^{k-1} - x^*\|^2 \leq \frac{1}{\mu} (x^{k-1} - x^*)^T \nabla g(x^{k-1}).
\end{equation}
Mithilfe von Ungleichung \eqref{eq:lyapunov_sim6} erhalten wir die Abschätzung
\begin{equation}\label{eq:lyapunov_final}
\begin{aligned}
\mathbb{E}[Q(\theta^k)\vert \mathcal{F}_{k-1}] - (1 - \delta)Q(\theta^{k-1}) 
&\leq -\left(2\alpha - 3\alpha^2 n L + \frac{\delta^2 \left(1 - \frac{1}{n}\right)^2}{3n\delta -1 -2\delta +\frac{\delta -1}{n}}\frac{n}{\mu} - \frac{\delta}{\mu}\right) 
\\&\ \ (x^{k-1} - x^*)^T \nabla g(x^{k-1}).
\end{aligned}
\end{equation}

Um die lineare Konvergenz der Folge $\left(\mathbb{E}Q(\theta^k)\right)_{k\in\mathbb{N}}$, d.h.
\begin{equation*}
\mathbb{E}Q(\theta^k) \leq (1-\delta)\mathbb{E}Q(\theta^{k-1}),
\end{equation*}
zu beweisen, zeigen wir, dass die rechte Seite von Abschätzung \eqref{eq:lyapunov_final} negativ ist. Aufgrund von Ungleichung \eqref{eq:lyapunov_sim6} wissen wir, dass $(x^{k-1} - x^*)^T \nabla g(x^{k-1})$ positiv ist. Daher muss nur noch  die Positivität von $\left(2\alpha - 3\alpha^2 n L + \frac{\delta^2 \left(1 - \frac{1}{n}\right)^2}{3n\delta -1 -2\delta +\frac{\delta -1}{n}}\frac{n}{\mu} - \frac{\delta}{\mu}\right)$ gezeigt werden.
Mit der Wahl $\delta = \frac{\mu}{8 n L}$ und der Schrittweite $\alpha = \frac{1}{2 n L}$ erhält man
\begin{equation}
\begin{aligned}
2\alpha - 3\alpha^2 n L + \frac{\delta^2 \left(1 - \frac{1}{n}\right)^2}{3n\delta -1 -2\delta +\frac{\delta -1}{n}}\frac{n}{\mu} - \frac{\delta}{\mu} &= \frac{1}{nL} - \frac{3}{4nL} - \frac{1}{8nL} - \frac{\delta^2(1-\frac{1}{n})^2\frac{n}{\mu}}{1 - 3n\delta + 2\delta + \frac{1-\delta}{n}} \\
&\geq \frac{1}{8nL} - \frac{\delta^2\frac{n}{\mu}}{1-3n\delta} \\
&= \frac{1}{8nL} - \frac{\frac{\mu}{64 nL^2}}{1-\frac{3\mu}{8L}} \\
&\geq \frac{1}{8nL} - \frac{\frac{\mu}{64 nL^2}}{1-\frac{3}{8}} \\
&= \frac{1}{8nL} - \frac{\mu}{40 nL^2} \\
& \geq \frac{1}{8nL} - \frac{1}{40 nL} \\
&\geq 0.
\end{aligned}
\end{equation}
Somit gilt 
\begin{equation*}
\mathbb{E}\left[Q(\theta^k)\vert \mathcal{F}_{k-1}\right] - (1-\delta)Q(\theta^{k-1}) \leq 0.
\end{equation*}
Durch die Bildung des Erwartungswertes erhalten wir das gewünschte Ergebnis
\begin{equation*}
\mathbb{E}Q(\theta^k) - (1-\delta)\mathbb{E}Q(\theta^{k-1}) \leq 0.
\end{equation*}
Die Folge $\left(\mathbb{E}Q(\theta^k)\right)_{k\in\mathbb{N}}$ konvergiert linear und es gilt
\begin{equation*}
\mathbb{E}Q(\theta^k) \leq \left(1 - \frac{\mu}{8nL}\right)^k \mathbb{E}Q(\theta^0).
\end{equation*}
$\hfill$
\newline
\textbf{2. Dominierung von $\|x^k - x^*\|^2$ durch $Q(\theta^k)$}
\newline
Für diesen Beweisschritt ist es hilfreich sich an die Definitionen von $\theta^k$ und $\theta^*$ zu erinnern: 
$$
\begin{aligned} 
\theta^k = \begin{pmatrix}
y_1^k \\ \vdots \\ y_n^k \\ x^k 
\end{pmatrix}
\in \mathbb{R}^{(n+1)p},
&
\theta^* = \begin{pmatrix}
\nabla f_1(x^*) \\ \vdots \\ \nabla f_n(x^*) \\ x^*
\end{pmatrix}
\in \mathbb{R}^{(n+1)p}.
\end{aligned}
$$

Wenn wir es schaffen zu zeigen, dass $\|x^k - x^*\|^2$ durch $Q(\theta^k)$ um einen Konstanten Faktor $\gamma > 0$ dominiert wird, d.h. dass die Ungleichung 
\begin{equation}\label{eq:domination}
\gamma \|x^k - x^*\|^2 \leq Q(\theta^k)
\end{equation}  
gilt, dann folgt daraus unmittelbar die lineare Konvergenz des Verfahrens. Mithilfe der Definitionen von $Q$, $\theta^k$ und $\theta^*$ kann Ungleichung \eqref{eq:domination} folgendermaßen geschrieben werden
\begin{equation}
\begin{aligned}
(\theta^k - \theta^*)^T \underbrace{\begin{pmatrix}0 & 0 \\ 0 & \gamma I \end{pmatrix}}_{:= R} (\theta^k - \theta^*)  
	\leq (\theta^k - \theta^*)^T \underbrace{\begin{pmatrix} A & b \\ b^T & c \end{pmatrix}}_{:= P} (\theta^k - \theta^*),
\end{aligned}
\end{equation}
was äquivalent zur Ungleichung 
\begin{equation}\label{eq:domination2}
\begin{aligned}
(\theta^k - \theta^*)^T (P - R) (\theta^k - \theta^*) \geq 0
\end{aligned}
\end{equation}
ist.
Offensichtlich gilt diese Ungleichung, wenn $(P - R)$ eine positiv definite Matrix ist. 

\cite{Roux2012ASG} zeigen die positive Definitheit der Matrix $(P - R)$ für $\gamma = \frac{1}{3}$  anhand der Schur-Komplement Bedingung.
%https://de.wikipedia.org/wiki/Schurkomplement
\begin{definition}\emph{(Schur-Komplement)}
Sei $M$ eine $(n+m) \times (n+m)$-Matrix, die aus vier Teilblöcken zusammengesetzt ist: 
\begin{equation*}
M = \begin{pmatrix} A & B \\ C & D \end{pmatrix}.
\end{equation*}
Dabei sei $A$ eine $n \times n$-, $B$ eine $n \times m$-, $C$ eine $m \times n$- und $D$ eine $m \times m$-Matrix. Des Weiteren sei vorausgesetzt, dass $A$ und $D$ invertierbar sind. Die Matrix
\begin{equation*}
M/A = D - C A^{-1} B
\end{equation*}
wird als Schur-Komplement von $A$ in $M$ bezeichnet und die Matrix
\begin{equation*}
M/D = A - B D^{-1} C
\end{equation*}
als Schur-Komplement von $D$ in $M$.
\end{definition}
%https://en.wikipedia.org/wiki/Schur_complement
\begin{proposition}\emph{(Schur-Komplement Bedingung für positive Definitheit)}
Sei $X$ eine symmetrische Matrix gegeben durch
\begin{equation*}
X = \begin{pmatrix} A & B \\ B^T & C \end{pmatrix},
\end{equation*}
wobei die Matrizen $A$ und $C$ invertierbar sind. Sei $X/A$ das Schur-Komplement von $A$ in $X$ und $X/C$ das Schur-Komplement von $C$ in $X$. 
Dann gilt:
\begin{itemize}
\item $X$ ist genau dann positiv definit, wenn $A$ und $X/A$ positiv definit sind.
\item $X$ ist genau dann positiv definit, wenn $C$ und $X/C$ positiv definit sind.
\end{itemize}
\end{proposition}
$\hfill$
\newline
Die Matrix $(P - R)$ ist eine symmetrische Matrix der Gestalt $\begin{pmatrix} A & b \\ b^T & c - \frac{1}{3}I \end{pmatrix}$ und $A$ ist positiv definit. Daher muss nun nur noch die positive Definitheit des Schur-Komplements $(P - R)/A$ nachgewiesen werden. 

Das Einsetzen der Definitionen von $A, b$ und $c$ in die Definition von $(P - R)/A$ liefert
\begin{equation}\label{eq:domination_schur}
\begin{aligned}
(P - R)/A &= \frac{2}{3} I - \alpha^2 \left(1 - \frac{1}{n}\right)^2 e^T \left[\left(3n\alpha^2 + \frac{\alpha^2}{n} - 2\alpha^2\right)\frac{ee^T}{n}\right]^{-1}e \\
&= \frac{2}{3} I - \frac{n\left(1 - \frac{1}{n}\right)^2}{3n + \frac{1}{n} - 2}\frac{ee^T}{n}.
\end{aligned}
\end{equation}
Es gilt
%http://math.stackexchange.com/questions/315382/what-are-the-succ-and-prec-operators-for-when-used-with-matrices
\begin{equation}
\begin{aligned}
\frac{2}{3} I - \frac{n\left(1 - \frac{1}{n}\right)^2}{3n + \frac{1}{n} - 2}\frac{ee^T}{n} &\succ \frac{2}{3} I - \frac{n}{3n - 2}\frac{ee^T}{n} \\
&\succ 0 \text{ für } n \geq 2.
\end{aligned}
\end{equation}
Wir schreiben dabei $M \succ N$ für quadratische Matrizen $M$ und $N$, wenn die Matrix $M - N$ positiv definit ist.

Daraus ergibt sich die positive Definitheit von $(P - R)$ und insbesondere
\begin{equation}\label{eq:domination_res}
\begin{aligned}
\mathbb{E}\|x^k - x^*\|^2 &\leq 3 \mathbb{E}Q(\theta^k) && \text{[Schritt 1]}\\ 
&\leq 3 \left(1 - \frac{\mu}{8 n L}\right)^k Q(\theta^0),
\end{aligned}
\end{equation}
wobei
\begin{equation}
\begin{aligned}
Q(\theta^0) &= 3n\alpha^2 \sum_{i=1}^n \|y_i^0 - \nabla f_i(x^*)\|^2 + \frac{(1 - 2n)\alpha}{n^2} \|\sum_{i=1}^n y_i^0\|^2 
\\&\ \ - 2 \alpha\left(1 - \frac{1}{n}\right)(x^0 - x^*)^T\left(\sum_{i=1}^n y_i^0\right) + \|x^0 - x^*\|^2 \\
&= \frac{3}{4nL^2} \sum_{i=1}^n \|y_i^0 - \nabla f_i(x^*)\|^2 
\\&\ \ + \frac{(1 - 2n)\alpha}{2 n^3 L} \|\sum_{i=1}^n y_i^0\|^2 - \frac{n - 1}{n^2 L}(x^0 - x^*)^T\left(\sum_{i=1}^n y_i^0\right) + \|x^0 - x^*\|^2 
\end{aligned}
\end{equation}
ist. Bei einer Initialisierung von $y_i^0 = 0$ für alle $i \in \{1, \dots, n\}$ gilt
\begin{equation}\label{eq:domination_q0}
Q(\theta^0) = \frac{3\sigma^2}{4L^2} + \|x^0 - x^*\|^2.
\end{equation}
Das Einsetzen von Gleichung \eqref{eq:domination_q0} in Ungleichung \eqref{eq:domination_res} liefert das gewünschte Resultat
\begin{equation}
\mathbb{E}\|x^k - x^*\|^2 \leq \left(1 - \frac{\mu}{8 n L}\right)^k\left(\frac{9\sigma^2}{4L^2} + 3\|x^0 - x^*\|^2\right).
\end{equation}
$\hfill \Box$
                                