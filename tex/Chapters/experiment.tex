\chapter{Numerische Ergebnisse}

In diesem Kapitel wollen wir überprüfen, ob sich die vorgestellten Verfahren in einem praxisrelevantem Beispiel wirklich so verhalten, wie es die theoretische Analyse vermuten lässt. Dazu lösen wir das überbestimmte Gleichungssystem, das bei der Diskretisierung der zirkulären Radontransformation auftritt, mithilfe der verschiedenen Verfahren. 

\section{Zirkuläre Radontransformation}

Die zirkuläre Radontransformation spielt bei diversen bildgebenden Verfahren in der Medizin eine große Rolle. Zum Beispiel bei der thermoakustischen und photoakustischen Tomographie. Aus physikalischer Perspektive funktionieren beide Verfahren ähnlich:
Ein Körperteil wird elektromagnetischer Strahlung ausgesetzt. Dabei wird ein Teil der Strahlung vom Körperteil absorbiert, das Gewebe erwärmt sich, durch die Erwärmung dehnt sich das Gewebe aus, was zur Entstehung einer akustischen Welle, die durch den Körperteil wandert, führt. Diese akustischen Wellen werden dann von mehreren Sensoren über einen gewissen Zeitraum gemessen. Aus den Messdaten wird das Bild der Hitzeabsorbtionsfunktion des Körperteils rekonstruieren. Dies wird mithilfe der zirkulären Radontransformation $g = R f$ 
\begin{equation}
g(x_0, y_0, r) = R f(x_0, y_0, r) = \int_{\partial B((x_0,y_0),r)} f(x,y) ds,
\end{equation} 
wobei $x_0, y_0 \in \mathbb{R}$ die Koordinaten eines Kreismittelpunkts in $\mathbb{R}^2$ sind, $r > 0$ der Radius des Kreises und $f: \mathbb{R}^2 \to \mathbb{R}$ eine Bildfunktion ist, modelliert. Die Rekonstruktion des Bildes der Hitzeabsorptionsfunktion entspricht der Rekonstruktion der Bildfunktion $f$ aus den Daten $R f$ entlang von Sphären verschiedener Radien, welche auf den Sensorpositionen zentriert sind. Typischerweise liegen die Sensoren auf einer Kreisbahn \citep{radon1,radon2}.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=1\linewidth]{Figures/circular_radon_small.png}
\caption{Schematische Darstellung der zirkulären Radontransformation. Die Sensoren sind entlang eines Kreises um das Objekt $f$ angeordnet. Für jede Position $a = (x_0, y_0)$ auf dem Kreis und jeden Radius $r$ liefert die zirkuläre Radontransformation $R f$ einen Wert $R f(a,r)$.}\label{fig:haltmeiersmiley}
\end{center}
\end{figure}

Die Rekonstruktion erfolgt querschnittsweise, die Querschnitte werden mithilfe der Messungen aus den verschiedenen Richtungen rekonstruiert. Die Verwendung von endlich vielen Sensoren entspricht einer Diskretisierung entlang der Kreisbahn auf der die Sensoren liegen, wird zusätzlich entlang der verwendeten Radien diskretisiert, so erhält man ein Gleichungssystem
\begin{equation}\label{eq:radongls}
\tilde{g} = \tilde{R} \tilde{f},
\end{equation}
wobei $\tilde{g}$ (Abbildung \ref{fig:haltmeiersmiley} rechts) eine Matrix, die die Funktionsauswertungen von $g$ für $a = (x_0, y_0)$ auf einer Kreisbahn und verschiedene Radien $r$ enthält, $\tilde{R}$ die Abbildungsmatrix der zirkulären Radontransformation und $\tilde{f}$ (Abbildung \ref{fig:haltmeiersmiley} links) die Bildmatrix ist. 

\subsection{Aufbau und Problemformulierung}

In unserem Anwendungsbeispiel rekonstruieren wir einen synthetisch erzeugten Querschnitt $\tilde{f}$, der die Konturen eines Smileys, siehe Abbildung \ref{fig:haltmeiersmiley} links, enthält. Dessen Radontransformierte, siehe Abbildung \ref{fig:haltmeiersmiley} rechts, entspricht den Messungen von 400 zyklisch um das Objekt angeordneten Sensoren für jeweils 300 Radien, und wurde durch die Anwendung der zirkulären Radontransformation auf $\tilde{f}$ berechnet. Die diskretisierte Radontransformierte $\tilde{g}$ liegt also in $\mathbb{R}^{400 \times 300}$.

Um ein Optimierungsproblem der gewünschten Form zu erhalten, machen wir uns die Problemstruktur zunutze und unterteilen das Gleichungssystem \eqref{eq:radongls} in Teilgleichungssysteme; eines pro Sensor. Es gibt nämlich für jeden Sensor mehrere Gleichungen, die den Auswertungen der Radontransformation auf Sphären von verschiedenen Radien entsprechen. Somit erhalten wir die Zielfunktion
\begin{equation}
g(\tilde{f}) = \frac{1}{400} \sum_{a = 1}^{400} \|\tilde{R}^a \tilde{f} - \tilde{g}^a\|_2^2,
\end{equation}
wobei $a \in \{1, \dots, 400\}$ die Diskretisierung entlang der Kreisbahn auf der die Sensoren liegen indiziert und $\tilde{R}^a$ beziehungsweise $\tilde{g}^a$ die zum Sensor $a$ gehörigen Teile des Gleichungssystems \eqref{eq:radongls}, d.h. die Gleichungen für die 300 zum Sensor $a$ gehörigen Radien, bezeichnen.

\subsection{Landweber-Kaczmarz Verfahren}

Ein etabliertes Verfahren um das Gleichungssystem \eqref{eq:radongls} aufzulösen ist das sogenannte Landweber-Kaczmarz Verfahren \citep{landweber1,landweber2}, welches durch die Iterationsvorschrift

\begin{equation}\label{eq:LKverfahren}
\begin{aligned}
x^0 &\in \mathbb{R}^p \\
x^{k+1} &= x^k - \alpha_k \nabla f_{j_k}(x^k) && \text{mit } j_k := k \bmod n 
\end{aligned}
\end{equation}
gegeben ist. Die Ähnlichkeit zum stochastischen Gradientenverfahren 
\begin{equation}
\begin{aligned}
x^0 &\in \mathbb{R}^p \\
x^{k+1} &= x^k - \alpha_k \nabla f_{i_k}(x^k) && \text{mit } i_k \sim \mathcal{U}_{\{1, \dots, n\}} 
\end{aligned}
\end{equation}
ist dabei unschwer zu erkennen. Die beiden Verfahren unterscheiden sich lediglich in der Auswahlmethode der Teilgradienten. Im Gegensatz zum stochastischen Gradientenverfahren geschieht dies beim Landweber-Kaczmarz Verfahren auf deterministische Weise. Die Teilgradienten werden dabei der Reihe nach gewählt.

\subsection{Numerische Experimente}

Wir vergleichen die folgenden Gradienten-Methoden zur Lösung eines überbestimmten Gleichungssystems, welches bei der Zirkulären Radontransformation auftritt:

\begin{enumerate}
\item \textbf{FGD}: Das traditionelle Gradientenverfahren beschrieben durch Iterationsvorschrift \eqref{eq:Gradientenverfahren}.
\item \textbf{LK}: Das Landweber-Kaczmarz Verfahren beschrieben durch Iterationsvorschrift \eqref{eq:LKverfahren}.
\item \textbf{SGD}: Das stochastische Gradientenverfahren beschrieben durch Iterationsvorschrift \eqref{eq:StochastischesGradientenverfahren}.
\item \textbf{SAG}: Das SAG-Verfahren beschrieben durch Iterationsvorschrift \eqref{eq:sag}, wobei die Standardinitialisierung $y^0 = 0$ verwendet wird.
\item \textbf{SAG*}: Das SAG-Verfahren beschrieben durch Iterationsvorschrift \eqref{eq:sag}, wobei $x^0$ und $y^0$ mithilfe eines Zyklus des stochastischen Gradientenverfahrens initialisiert werden.
\end{enumerate}

Ein Zyklus entspricht einem Schritt des traditionellen Gradientenverfahrens beziehungsweise $n$ Schritten der auf Teilgradienten basierenden Verfahren. In den Experimenten wurde für alle Verfahren die konstante Schrittweite $\alpha = 1$ verwendet.

\subsubsection{Iterationskosten}

Hinsichtlich der Iterationskosten verhalten sich die Verfahren in etwa wie erwartet. Das stochastische Gradientenverfahren ist in etwa 144 mal so schnell wie das traditionelle Gradientenverfahren und das SAG-Verfahren ist nur etwas langsamer als das stochastische Gradientenverfahren. In der Theorie sollte die Iteration des  stochastischen Gradientenverfahrens jedoch 400 mal so schnell sein wie die des traditionellen Verfahrens, da dort im Gegensatz zur Iteration des traditionellen Verfahrens nur einer der 400 Teilgradienten berechnet werden muss. Die Analyse der Berechnungskosten des Programmes lässt vermuten, dass diese Abweichung durch gewisse Overhead-Berechnungen in der Teilgradientenbildung zustande kommen. Eine Zusammenfassung der Iterationszeiten der verschiedenen Verfahren ist in Tabelle \ref{tab:iterationszeiten} zu finden.

\begin{table}[h!]
\begin{center}
\begin{tabular}{|c|c|}
\hline
\textbf{Methode} & \textbf{Zeit}\\
\hline 
FGD & 724 ms\\ %0.72394
\hline 
LK & 5 ms \\
\hline
SGD & 5 ms\\ %0.0046417
\hline 
SAG & 8 ms\\ % 0.0081851
\hline 
SAG* &  8 ms\\ %0.0076416
\hline 
\end{tabular} 
\end{center}
\caption{Vergleich der Iterationskosten der verschiedenen Verfahren in Millisekunden. Die Tabelle enthält die über 50 Zyklen gemittelten Iterationszeiten der verschiedenen Verfahren.}\label{tab:iterationszeiten}
\end{table}

\subsubsection{Konvergenzrate und Genauigkeit}

Abbildung \ref{fig:comparison} (a) stellt den Fehler der Iterierten dar, wobei die $x$-Achse in Zyklen unterteilt ist und die $y$-Achse dem integriertem Fehler der Iterierten entspricht.
Abbildung \ref{fig:comparison} (b) stellt den Fehler der Iterierten bezüglich der Zielfunktion dar, wobei die $x$-Achse in Zyklen unterteilt ist und die $y$-Achse dem zehner-logarithmiertem Fehler der Iterierten bezüglich der Zielfunktion enthält. Ein Zyklus in Abbildung \ref{fig:comparison} entspricht einem Schritt des traditionellen Gradientenverfahrens beziehungsweise 400 Schritten der auf Teilgradienten basierenden Verfahren.    
Bei der Betrachtung von Abbildung \ref{fig:comparison} (a) ist zu erkennen, dass die SAG-Verfahren (SAG und SAG*) in diesem Anwendungsbeispiel nach einer oszillierenden Startphase Q-linear konvergieren. Des Weiteren konvergiert das stochastische Gradientenverfahren (SGD) wie erwartet Q-sublinear. Das Landweber-Kaczmarcz Verfahren (LK) verhält sich ähnlich zum stochastischen Gradientenverfahren also Q-sublinear. Das traditionelle Gradientenverfahren sollte theoretisch auch Q-linear konvergieren, allerdings ist dies nicht oder nur schwer zu erkennen. 

Die oszillierende Startphase tritt nur bei den SAG-Verfahren auf, was vermutlich darauf zurückzuführen ist, dass durch das Speichern der aktuellsten Versionen der Gradienten vor allem anfangs, wo die Iterierten noch schlecht sind, unter anderem "`falsche"' Richtungen akkumuliert werden. Kann man sich nur wenige Iterationen leisten, so ist das stochastische Gradientenverfahren also zu bevorzugen. Die Initialisierung der Teilgradienten des SAG-Verfahrens durch einen Zyklus des stochastischen Gradientenverfahrens wirkt der oszillierenden Startphase kaum entgegen und führt in unserem Fall auch zu keinen sonstigen Verbesserungen. Eventuell ist eine längere Initialisierungsphase notwendig um einen Effekt zu beobachten. Dies wäre in diesem Anwendungsbeispiel durchaus möglich, da das stochastische Gradientenverfahren hier in den ersten fünf bis sechs Zyklen ohnehin besser abschneidet als das SAG-Verfahren. Es stellt sich allerdings die Frage, ob das dieses Verhalten der Regelfall ist. %Eventuell betrachten erg von SAG paper.

Des Weiteren fällt auf, dass die auf Teilgradienten basierenden Verfahren schneller konvergieren als das traditionelle Gradientenverfahren und dass die stochastischen Verfahren schneller konvergieren als die deterministischen. Ersteres ist auf die Tatsache zurückzuführen, dass ein Zyklus des traditionellen Gradientenverfahrens einer Verbesserung der Iterierten entspricht, während ein Zyklus der auf Teilgradienten basierenden Verfahren $n = 400$ Verbesserungen der Iterierten entspricht. Obwohl aus der Analyse der Iterationszeiten, speziell aus der Beobachtung, dass beispielsweise eine Iteration des stochastischen Gradientenverfahrens nur $144$ mal so schnell ist wie eine Iteration des traditionellen Gradientenverfahrens, hervorgeht, dass es sich hierbei um einen nicht ganz fairen Vergleich handelt, ist dieser Trend deutlich zu erkennen. 

In Abbildung \ref{fig:comparison_prec} werden die Verfahren hinsichtlich ihrer Genauigkeit verglichen, die $x$-Achse enthält dabei die zu erreichende Genauigkeit und die $y$-Achse die dafür notwendige Anzahl von Zyklen. Wie in Abbildung \ref{fig:comparison} ist auch hier zu erkennen, dass die SAG-Verfahren zwar die höchste Genauigkeit erreichen, jedoch anfangs aufgrund der Oszillationen langsamer sind als das stochastische Gradientenverfahren und als das Landweber-Kaczmarz Verfahren. 

Weiters ist in Abbildung \ref{fig:comparison_prec} (a) deutlich zu erkennen, dass das stochastische Gradientenverfahren in diesem Anwendungsfall besser funktioniert als das Landweber-Kaczmarz Verfahren, da es innerhalb der betrachteten Anzahl von Zyklen um eine ganze Nachkommastelle genauer ist. Diese Beobachtung ist insofern bedeuten, als das Landweber-Kaczmarz Verfahren ein gängiges Verfahren für die betrachtete Anwendungsdomäne ist. 

Die Dominanz des stochastischen Gradientenverfahren gegenüber des Landweber-Kaczmarz Verfahrens spiegelt sich auch schon im Startverhalten der beiden Verfahren wieder. Dazu betrachten wir zusätzlich zu Abbildung \ref{fig:comparison_prec} die Tabellen \ref{tab:startverhalten} (a) und \ref{tab:startverhalten} (b), welche die Iterierten der beiden Verfahren in Abständen von jeweils fünf Iterationen enthalten. Aus der Praxis ist bekannt, dass das Landweber-Kaczmarz Verfahren in etwa $\frac{n}{2} = 200$ Iterationen braucht um die Konturen eines Querschnittes zu rekonstruieren. Dieses Verhalten ist auch in Tabelle \ref{tab:startverhalten} zu erkennen. Im Gegensatz dazu benötigt das stochastische Gradientenverfahren nur etwa halb so viele Iterationen um Konturen in der augenscheinlich selben Qualität zu rekonstruieren. 

Auch die Startschwierigkeiten des SAG-Verfahrens sind in Tabelle \ref{tab:startverhalten} (c) zu erkennen, die intensiven Gelb-töne weisen darauf hin, dass die Approximation des Gradienten im SAG-Verfahrens anfangs in eine dominante Richtung weist. 

\begin{figure}[h!]
    \begin{subfigure}[b]{1\linewidth}
	    \includegraphics[width=0.9\linewidth]{Figures/experiment1wh/it_50.png}
		\caption{Fehler der Iterierten}
	\end{subfigure}
	\begin{subfigure}[b]{1\linewidth}
	    \includegraphics[width=0.9\linewidth]{Figures/experiment1wh/zf_50.png}
		\caption{Fehler bezüglich Zielfunktion}
	\end{subfigure}
\caption{Vergleich der vorgestellten Methoden bezüglich der Fehler der Iterierten und der Fehler der Zielfunktion am Beispiel der zirkulären Radontransformation. Die $y$"~Achse ist logarithmisch skaliert. Die $x$-Achse ist in Zyklen unterteilt. Ein Zyklus entspricht dabei einem Durchlauf durch die Daten, also einer Iteration des Gradientenverfahrens oder $n$ Iterationen der anderen Verfahren.}\label{fig:comparison}
\end{figure}

%\begin{figure}[h!]
%    \begin{subfigure}[b]{1\linewidth}
%	    \includegraphics[width=0.9\linewidth]{Figures/experiment1/it_9.png}
%		\caption{Fehler der Iterierten}
%	\end{subfigure}
%	\begin{subfigure}[b]{1\linewidth}
%	    \includegraphics[width=0.9\linewidth]{Figures/experiment1/zf_9.png}
%		\caption{Fehler bezüglich Zielfunktion}
%	\end{subfigure}
%\caption{Vergleich des Startverhaltens der vorgestellten Methoden. Die $Y$-Achse ist logarithmisch skaliert. Die $X$-Achse ist in Zyklen unterteilt. Ein Zyklus entspricht dabei einem durchlauf durch die Daten, i.e. einer Iteration des Gradientenverfahrens oder $n$ Iterationen der anderen Verfahren.}\label{fig:comparison_start}
%\end{figure}

\begin{figure}[h!]
    \begin{subfigure}[b]{1\linewidth}
	    \includegraphics[width=0.9\linewidth]{Figures/experiment1wh/it_genauigkeit_lines.png}
		\caption{Genauigkeit der Iterierten}
	\end{subfigure}
	\begin{subfigure}[b]{1\linewidth}
	    \includegraphics[width=0.9\linewidth]{Figures/experiment1wh/zf_genauigkeit_lines.png}
		\caption{Genauigkeit bezüglich Zielfunktion}
	\end{subfigure}
\caption{Vergleich der vorgestellten Methoden bis eine gewisse Genauigkeit erreicht ist. Die Abbildung stellt die Anzahl der Zyklen dar, die erforderlich sind um eine gewisse Genauigkeit zu erreichen.}\label{fig:comparison_prec}
\end{figure}

\begin{table}
\begin{subtable}[b]{\linewidth}
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{cccccccccc}
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_1.png} & 
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_6.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_11.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_16.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_21.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_26.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_31.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_36.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_41.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_46.png} \\
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_51.png} & 
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_56.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_61.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_66.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_71.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_76.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_81.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_86.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_91.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_96.png} \\
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_101.png} & 
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_106.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_111.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_116.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_121.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_126.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_131.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_136.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_141.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_146.png} \\
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_151.png} & 
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_156.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_161.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_166.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_171.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_176.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_181.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_186.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_191.png} &
\includegraphicstotab{Figures/smileys/landweber-kaczmarz_196.png}
\end{tabular}%
}
\caption{Landweber-Kaczmarz Verfahren}
\end{subtable}
\begin{subtable}[b]{\linewidth}
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{cccccccccc}
\includegraphicstotab{Figures/smileys/stochastic_1.png} & 
\includegraphicstotab{Figures/smileys/stochastic_6.png} &
\includegraphicstotab{Figures/smileys/stochastic_11.png} &
\includegraphicstotab{Figures/smileys/stochastic_16.png} &
\includegraphicstotab{Figures/smileys/stochastic_21.png} &
\includegraphicstotab{Figures/smileys/stochastic_26.png} &
\includegraphicstotab{Figures/smileys/stochastic_31.png} &
\includegraphicstotab{Figures/smileys/stochastic_36.png} &
\includegraphicstotab{Figures/smileys/stochastic_41.png} &
\includegraphicstotab{Figures/smileys/stochastic_46.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic_51.png} & 
\includegraphicstotab{Figures/smileys/stochastic_56.png} &
\includegraphicstotab{Figures/smileys/stochastic_61.png} &
\includegraphicstotab{Figures/smileys/stochastic_66.png} &
\includegraphicstotab{Figures/smileys/stochastic_71.png} &
\includegraphicstotab{Figures/smileys/stochastic_76.png} &
\includegraphicstotab{Figures/smileys/stochastic_81.png} &
\includegraphicstotab{Figures/smileys/stochastic_86.png} &
\includegraphicstotab{Figures/smileys/stochastic_91.png} &
\includegraphicstotab{Figures/smileys/stochastic_96.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic_101.png} & 
\includegraphicstotab{Figures/smileys/stochastic_106.png} &
\includegraphicstotab{Figures/smileys/stochastic_111.png} &
\includegraphicstotab{Figures/smileys/stochastic_116.png} &
\includegraphicstotab{Figures/smileys/stochastic_121.png} &
\includegraphicstotab{Figures/smileys/stochastic_126.png} &
\includegraphicstotab{Figures/smileys/stochastic_131.png} &
\includegraphicstotab{Figures/smileys/stochastic_136.png} &
\includegraphicstotab{Figures/smileys/stochastic_141.png} &
\includegraphicstotab{Figures/smileys/stochastic_146.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic_151.png} & 
\includegraphicstotab{Figures/smileys/stochastic_156.png} &
\includegraphicstotab{Figures/smileys/stochastic_161.png} &
\includegraphicstotab{Figures/smileys/stochastic_166.png} &
\includegraphicstotab{Figures/smileys/stochastic_171.png} &
\includegraphicstotab{Figures/smileys/stochastic_176.png} &
\includegraphicstotab{Figures/smileys/stochastic_181.png} &
\includegraphicstotab{Figures/smileys/stochastic_186.png} &
\includegraphicstotab{Figures/smileys/stochastic_191.png} &
\includegraphicstotab{Figures/smileys/stochastic_196.png} 
 \\
\end{tabular}
}
\caption{Stochastisches Gradientenverfahren}
\end{subtable}
\begin{subtable}[b]{1\linewidth}
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{cccccccccc}
\includegraphicstotab{Figures/smileys/stochastic-avg_1.png} & 
\includegraphicstotab{Figures/smileys/stochastic-avg_6.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_11.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_16.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_21.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_26.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_31.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_36.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_41.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_46.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic-avg_51.png} & 
\includegraphicstotab{Figures/smileys/stochastic-avg_56.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_61.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_66.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_71.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_76.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_81.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_86.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_91.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_96.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic-avg_101.png} & 
\includegraphicstotab{Figures/smileys/stochastic-avg_106.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_111.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_116.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_121.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_126.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_131.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_136.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_141.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_146.png} 
 \\
\includegraphicstotab{Figures/smileys/stochastic-avg_151.png} & 
\includegraphicstotab{Figures/smileys/stochastic-avg_156.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_161.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_166.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_171.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_176.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_181.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_186.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_191.png} &
\includegraphicstotab{Figures/smileys/stochastic-avg_196.png} 
 \\
\end{tabular}
}
\caption{SAG Verfahren}
\end{subtable}
\caption{Illustration der Startphase der approximativen Verfahren. Zwischen den einzelnen Bildern liegen jeweils fünf Iterationen der zugehörigen Verfahren. Für jedes Verfahren werden also die Rekonstruktionen von Iteration $1$ bis $196$ dargestellt.}\label{tab:startverhalten}
\end{table}